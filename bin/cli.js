#!/usr/bin/env node
const cli = require("minimist")(process.argv.splice(1)),
    logger = new (require("trg_logger"))(),
    util = require("util"),
    EmbedBot = require("../EmbedBot");
if (util.isString(cli.token)) {
    let bot = new EmbedBot(cli.token);
    bot.start();
} else {
    logger.error("Missing --token");
}