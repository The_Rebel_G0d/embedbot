const
    fs = require("fs"),
    path = require("path"),
    os = require("os"),
    util = require("util"),
    Command = require("../Classes/Command"),
    logger = new (require("trg_logger"))("Command - embed"),
    request = require("requestretry");

module.exports = new (require("../Classes/Command"))({
    trigger: "purge",
    handle: (message, message_helper, bot) => {
        if (message.member.hasPermission("MANAGE_MESSAGES")) {
            if (message_helper.args[0] >> 0) {
                message.channel.fetchMessages({limit: message_helper.args[0], before: message.id}).then(messages => {
                    message_helper.edit("*working...*");
                    let promises = [];
                    messages.array().forEach(x => promises.push(x.delete()));
                    Promise.all(promises).then(() => {
                        message_helper.edit("**Done.**");
                    }).catch(err => {
                       logger.error("**Purge failed**");
                    });
                }).catch(err => {
                    message_helper.edit("Failed to fetch messages to delete");
                    logger.error("Failed to fetch messages to delete", err);
                });
            }
        } else {
            message_helper.edit("*I don't have permission to do that*");
        }
    }
});
