const
    fs = require("fs"),
    path = require("path"),
    os = require("os"),
    util = require("util"),
    Command = require("../Classes/Command"),
    logger = new (require("trg_logger"))("Command - embed"),
    request = require("requestretry");

module.exports = new (require("../Classes/Command"))({
    trigger: "roles",
    handle: (message, message_helper, bot) => {
	message.delete().catch(err => {});
        message.channel.send(`\`\`\`js\n${JSON.stringify(message.guild.roles.map(x => {return {name: x.name, id: x.id}}), null, 2)}\`\`\``);
    }
});
