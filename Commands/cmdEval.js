const
    fs = require("fs"),
    path = require("path"),
    os = require("os"),
    util = require("util"),
    Command = require("../Classes/Command"),
    logger = new (require("trg_logger"))("Command - embed"),
    request = require("requestretry");

module.exports = new (require("../Classes/Command"))({
    trigger: "eval",
    handle: (message, message_helper, bot) => {
        try {message.channel.send(`\`\`\`js\n${util.inspect(eval(message_helper.args.join(" "))).substr(0, 2000-9)}\`\`\``);} catch(e) {message.channel.send("Failed to eval");logger.error(e)}
    }
});
