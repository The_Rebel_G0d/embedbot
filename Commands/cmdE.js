const
    fs = require("fs"),
    path = require("path"),
    os = require("os"),
    util = require("util"),
    Command = require("../Classes/Command"),
    logger = new (require("trg_logger"))("Command - embed"),
    request = require("requestretry");

module.exports = new (require("../Classes/Command"))({
    trigger: "e",
    handle: (message, message_helper, bot, ipcHelper) => {
        if (message_helper.args.length == 0 || message_helper.args[0].toLowerCase().trim() == "help") {
            message_helper.reply("*missing arguments*");
        } else {
            if (ipcHelper) {
                let emote = /([a-z0-9:]+)/i.exec(message_helper.args[0])[1],
                    twitchEmote = /^<:([a-z]+):[0-9]+>:?(.*)?/i.exec(message_helper.args[0]),
                    _emote = emote + "",
                    _index = -1,
                    _command = "";

                if (twitchEmote != null && util.isString(twitchEmote[1]) && twitchEmote[1].length > 0) {
                    _emote = twitchEmote[1];
                    _index = 0;
                    _command = "";
                    if (util.isString(twitchEmote[2]) && twitchEmote[2].length > 0) {
                        _index = Number(twitchEmote[2]);
                        if (isNaN(_index)) {
                            _index = -1;
                            _command = twitchEmote[2];
                        }
                    }
                } else if (emote.includes(":")) {
                    let _parts = emote.split(":");
                    _emote = _parts.shift();
                    _index = Number(_parts[0]);
                    if (isNaN(_index)) {
                        _index = -1;
                    } else {
                        _parts.shift();
                    }
                    _command = _parts.join(":");
                }

                message_helper.edit(`*Fetching ${_emote}*`);
                ipcHelper.getEmote(_emote).then(data => {
                    if (data === null) {
                        message_helper.edit(`***\`${_emote}\` doesn't exist on the Nexus***`);
                    } else {
                        message_helper.editThenDelete(`*Done. Will embed shortly*`, 1);
                        require("../Classes/Embedder").EmbedEmote(message.channel, data.images[_command.toLowerCase() || "medium"], data.code);
                    }
                }).catch(err => {
                    console.error(err);
                    message_helper.editThenDelete("*IPC Failed*");
                })
            } else {
                message_helper.editThenDelete("*IPC Not Connected*");
            }
        }
    }
});
