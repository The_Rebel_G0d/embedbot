const
    fs = require("fs"),
    path = require("path"),
    os = require("os"),
    util = require("util"),
    Command = require("../Classes/Command"),
    logger = new (require("trg_logger"))("Command - embed"),
    request = require("requestretry");

module.exports = new (require("../Classes/Command"))({
    trigger: "embed",
    handle: (message, message_helper, bot) => {
        if (message_helper.args.length == 0 || message_helper.args[0].toLowerCase().trim() == "help") {
            message_helper.reply("*missing arguments*");
        } else {
            message_helper.editThenDelete("Embedding...");
            require("../Classes/Embedder").Embed(message.channel, message_helper.args);
        }
    }
});
