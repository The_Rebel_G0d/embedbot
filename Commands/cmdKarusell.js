const
    fs = require("fs"),
    path = require("path"),
    os = require("os"),
    util = require("util"),
    Command = require("../Classes/Command"),
    logger = new (require("trg_logger"))("Command - embed"),
    Request = require("../Classes/Request");

module.exports = new (require("../Classes/Command"))({
    trigger: "karusell",
    handle: (message, helper, bot) => {
        //<karusell e621 feral human type:gif
        if (!message.channel.nsfw) {
            return helper.edit("***Not an NSFW channel***");
        }
        if (helper.args.length > 1) {
            let _module = helper.args.shift();
            helper.edit(`*Fetching \`${helper.args.join(" ")}\` from \`${_module}\`*`);
            new Request(`https://karusell.trg0d.com/fetch/${_module}/${encodeURIComponent(helper.args.join(" "))}`).getJSON().then(data => {
                if (data && data.success) {
                    if (data.result && data.result.images.length) {
                        let URLs = data.result.images.map(x => x.url);
                        let buffer = [];
                        URLs.forEach((url, index) => {
                            buffer.push(url);
                            if (buffer.length == 5) {
                                helper.reply(buffer.join("\n").substr(0, 2000));
                                buffer = [];
                            }
                        });
                    } else {
                        helper.edit(`*No images for \`${helper.args.join(" ")}\` on \`${_module}\`*`);
                    }
                } else {
                    helper.edit("***API Failed***");
                    logger.error(data);
                }
            }).catch(err => {
                helper.edit("***Request Failed***");
                logger.error(err);
            });
        } else {
            helper.edit("***Missing args***");
        }
    }
});
