const
    fs = require("fs"),
    path = require("path"),
    os = require("os"),
    util = require("util"),
    Command = require("../Classes/Command"),
    logger = new (require("trg_logger"))("Command - embed"),
    request = require("requestretry");
const comDB = new (require("json-settings"))("data/customCommands.json");

module.exports = new (require("../Classes/Command"))({
    trigger: "cc",
    handle: (message, message_helper, bot) => {
        /*
        cc-
            add-
            delete-
            modify-
        */
        if (message_helper.args.length >= 2) {
            switch (message_helper.args[0].toLowerCase().trim()) {
                case "add": //[0] "add", [1] "someName", [2-N] "some command response".split(" ")
                    if (message_helper.args[2]) {
                        if (comDB.get(message_helper.args[1])) {
                            message_helper.editThenDelete("*Already exists. Use _edit_*");
                        } else {
                            comDB.set(message_helper.args[1], message_helper.args.slice(2).join(" "));
                            message_helper.editThenDelete("*Done*");
                        }
                    } else {
                        message_helper.editThenDelete("*Missing command content*");
                    }
                    break;
                case "del":
                case "delete":
                    comDB.delete(message_helper.args[1]);
                    message_helper.editThenDelete("*Done*");
                    break;
                case "modify":
                case "mod":
                case "change":
                case "chng":
                    if (message_helper.args[2]) {
                        if (comDB.get(message_helper.args[1])) {
                            comDB.set(message_helper.args[1], message_helper.args.slice(2).join(" "));
                            message_helper.editThenDelete("*Done*");
                        } else message_helper.editThenDelete("*Doesn't exist*");
                    } else message_helper.editThenDelete("*Missing key*");
                    break;
            }
        } else {
            switch (message_helper.args[0].toLowerCase().trim()) {
                case "list":
                case "ls":
                    message_helper.edit(Object.keys(comDB.raw_data).sort((a, b) => a.toLocaleLowerCase().localeCompare(b.toLocaleLowerCase())).join(", ").substr(0, 2000));
                    break;
            }
        }
    }
});
