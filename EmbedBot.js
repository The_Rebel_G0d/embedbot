const fs = require("fs"),
    path = require("path"),
    util = require("util"),
    logger = new (require("trg_logger"))("EmbedBot"),
    Discord = require("discord.js"),
    globby = require("globby"),
    EventEmitter = require("events").EventEmitter,
    Command = require("./Classes/Command"),
    MessageHelper = require("./Classes/MessageHelper");
require("promise.prototype.finally").shim();

const ccDB = new (require("json-settings"))("data/customCommands.json");
let ipc = require("node-ipc");

class EmbedBot {
    constructor(token) {
        EventEmitter.call(this);
        handleIPC(this);
        this.ipcHelper = new IPCHelper(ipc);
        this.token = token;
        this.bot = new Discord.Client();
        this.commands = new (require("binary-search-tree").BinarySearchTree)({
            unique: true,
            compareKeys: (a, b) => a.toLocaleLowerCase().localeCompare(b.toLocaleLowerCase())
        });

        let self = this;
        this.bot.on("ready", () => {
            self.emit("bot.ready");
            logger.info("Bot Ready");
        });
        this.bot.on("message", (msg) => {
            self.emit("bot.message", msg);
            if (msg.content.charAt(0) == "<" && msg.author.id == "75746323105579008") {
                let split = msg.content.substr(1).split(" ");
                let trigger = split.shift();
                let cmds = self.commands.search(trigger);
                if (cmds && cmds[0]) {
                    cmds[0].handle(msg, new MessageHelper(msg), self.bot, self.ipcHelper);
                    self.emit("CommandFired", trigger, msg);
                } else {
                    let fromCC = ccDB.get(trigger);
                    if (fromCC) {
                        new MessageHelper(msg).edit(`***${trigger}***\n${fromCC}`);
                        self.emit("CommandFired", trigger, msg);
                    }
                }
            }
        });
        this.bot.on("error", (err, id) => {
            self.emit("bot.error", err, id);
            if (id != null) {
                logger.error(`Shard ${id} errored.`, err);
            } else {
                logger.error("Bot error", err);
            }
        });
    }

    start() {
        if (this.started === true) return;
        this.started = true;

        let self = this;
        globby(`${path.resolve(".")}${path.sep}Commands${path.sep}**${path.sep}*.js`).then(data => {
            if (util.isArray(data)) {
                for (let i = 0; i < data.length; i++) {
                    let temp = require(data[i]);
                    if (util.isString(temp.trigger) && util.isFunction(temp.handle) && temp.disabled !== true) {
                        self.commands.insert(temp.trigger, temp);
                        logger.info(`got command '${temp.trigger}'`);
                        self.emit("NewCommand", temp.trigger);
                    } else {
                        self.emit("InvalidCommand", data[i]);
                    }
                }
            }
        }).catch(err => {
            logger.error("Failed to glob commands", err);
        }).finally(() => {
            self.bot.login(self.token);
        });
    }

    stop() {
        this.started = false;
        this.bot.disconnect();
        logger.warn("BST not emptied, old commands will not be reloaded");
    }

    setToken(token) {
        this.token = token;
        this.emit("NewToken", token);
    }
}
util.inherits(EmbedBot, EventEmitter);
module.exports = EmbedBot;

function handleIPC(emitter) {
    ipc.config.id = "EmbedBot";
    ipc.config.appspace = "trg0d.ipc.";
    ipc.config.logger = require("debug")("IPC");

    ipc.connectTo("EmoteNexus", function() {
        ipc.of.EmoteNexus.on("emote.response", data => {
            emitter.emit("ipc.emote.response", data);
        });
    });
}

class IPCHelper {
    constructor(ipc) {
        this.ipc = ipc;
        this.ipcListeners = {};
        if (ipc && ipc.of && ipc.of.EmoteNexus) {
            ipc.of.EmoteNexus.on("emote.response", data => {
                if (data.discrim) {
                    if (this.ipcListeners[data.discrim]) {
                        this.ipcListeners[data.discrim](data.emote);
                        delete this.ipcListeners[data.discrim];
                    }
                }
            })
        } else {
            throw new Error("Invalid IPC Supplied");
        }
    }

    getEmote(emote) {
        return new Promise(function(resolve, reject) {
            if (this.ipc && this.ipc.of && this.ipc.of.EmoteNexus) {
                let discrim = `${Date.now()}-${Math.random() * 1e9 >> 0}`;
                this.ipcListeners[discrim] = resolve;
                this.ipc.of.EmoteNexus.emit("emote.request", {discrim, emote});
            } else {
                reject(new Error("IPC down?"));
            }
        }.bind(this));
    }
}