const
    request = require("requestretry"),
    util = require("util"),
    logger = new (require("trg_logger"))("Request");

const
    {version} = require("../package.json") || "0.0.0",
    defaults = {
        headers: {
            "User-Agent": `The_Rebel_G0D/EmoteBot v${version} (rebel@trg0d.me)`,
            "X-Abuse-Contact": "rebel@trg0d.me"
        },
        maxAttempts: 3,
        retryDelay: 2500,
        retryStrategy: request.RetryStrategies.HTTPOrNetworkError,
        fullResponse: false
    };
class Request {
    /**
     *
     * @param {String} url
     * @param {Object} [options]
     */
    constructor(url, options) {
        if (util.isNullOrUndefined(url) && util.isNullOrUndefined(options)) throw new Error("Missing URL/options");
        if (!util.isString(url) && !util.isObject(options)) throw new Error("Invalid URL/Options");
        if (util.isNullOrUndefined(options) && util.isObject(url)) {
            options = url;
            url = null;
        }
        if (!util.isString(url)) {
            if (!util.isObject(options)) throw new Error("Missing URL");
            if (!util.isString(options.url)) throw new Error("Missing URL"); else this.url = options.url;
        }
        this.options = Object.assign({url}, defaults, options);
    }

    get url() {
        return this.options.url;
    }

    set url(val) {
        this.options.url = val;
    }

    /**
     * Sets up the request for a twitch API request
     * 
     * @returns {Request}
     */
    twitch(clientID) {
        this.options.headers["Accept"] = "application/vnd.twitchtv.v5+json";
        this.options.headers["Client-ID"] = clientID;
        console.info(this.options.headers);
        return this;
    }

    /**
     *
     * @returns {Promise}
     */
    get() {
        logger.debug("REQUEST: [GET] %s", this.url);
        this.options.method = "GET";
        return request(this.options);
    }

    getRaw() {
        logger.debug("REQUEST: [GET-RAW] %s", this.url);
        this.options.encoding = null;
        this.options.method = "GET";
        return request(this.options);
    }

    getJSON() {
        this.options.method = "GET";
        logger.debug("REQUEST: [GET-JSON] %s", this.url);
        let options = this.options;
        return new Promise(function(resolve, reject) {
            request(options).then(data => {
                try {
                    if (data) {
                        let toRet = data;
                        try {
                            toRet = JSON.parse(data);
                        } catch (e) {
                            console.info(toRet);
                            return reject(new Error("Data was not valid JSON"));
                        }
                        resolve(toRet);
                    } else {
                        reject(new Error("Got invalid body"));
                    }
                } catch (e) {
                    reject(e);
                }
            }).catch(reject);
        });
    }

    /**
     *
     * @param {String|Object} body
     * @returns {Promise}
     */
    postJSON(data) {
        this.options.json = util.isString(data) ? JSON.parse(data) : data;
        this.options.method = "POST";
        logger.debug("REQUEST: [POST] %s", this.url);
        return request(this.url, this.options);
    }

    /**
     *
     * @param {String|Object} data
     * @returns {Promise}
     */
    putJSON(data) {
        this.options.json = util.isString(data) ? JSON.parse(data) : data;
        this.options.method = "PUT";
        logger.debug("REQUEST: [PUT] %s", this.url);
        return request(this.url, this.options);
    }
}
module.exports = Request;