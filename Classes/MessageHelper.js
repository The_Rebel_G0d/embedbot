const util = require("util");
const Message = (require("discord.js").Message);

const noop = () => {};

class MessageHelper {
    /**
     * @param message {Message}
     */
    constructor(message) {
        if (!message instanceof Message)
            throw new TypeError("Invalid message type. Was expecting a Discord.js message.");

        this.message = message;

        let split = message.content.split(" ");
        this.command = split.shift().substr(1);
        this.args = split;
    }

    reply(message) {
        this._createMessage(message);
    }

    edit(newMessage) {
        this.message.edit(newMessage).catch(noop);
    }

    editThenDelete(newMessage, deleteTimeout = 3) {
        this.message.edit(newMessage).then(msg => {
            msg.delete((deleteTimeout >> 0 || 3) * 1000).catch(noop);
        }).catch(noop);
    }

    embed(embed_obj) {
        if (util.isObject(embed_obj)) {
            if (!util.isObject(embed_obj["embed"])) {
                embed_obj = {embed: embed_obj};
            }
        } else {
            embed_obj = MessageHelper.MakeRichStatus(embed_obj);
        }
        this._createMessage(embed_obj);
    }

    /**
     *
     * @param command {Command}
     */
    canCallerExecute(command) {
        //check if user is a superuser => execute : else =>
        //check command's user whitelist AND channel whitelist
    }

    deleteMessage() {
        this.message.delete().catch(noop);
    }

    _createMessage(message) {
        this.message.reply(message).catch(noop);
    }

    get authorID() {
        return this.message.author.id;
    }

    get channelID() {
        return this.message.channel.id;
    }

    get messageID() {
        return this.message.id;
    }

    get guildID() {
        return this.message.channel.guild.id;
    }

    static MakeSuccessMsg(message) {
        message = `**${MessageHelper.SuccessIcon()} ${message}**`;
        return MessageHelper.MakeRichStatus(message, 0x00ff00);
    }

    static MakeWarnMsg(message) {
        message = `**${MessageHelper.WarnIcon()} ${message}**`;
        return MessageHelper.MakeRichStatus(message, 0xffff00);
    }

    static MakeErrorMsg(message) {
        message = `**${MessageHelper.ErrorIcon()} ${message}**`;
        return MessageHelper.MakeRichStatus(message, 0xff0000);
    }

    static MakeRichStatus(message, color = 0xffffff) {
        return {
            embed: {
                type: "rich",
                description: message,
                color
            }
        };
    }

    static ErrorIcon() {
        return "\u274C";
    }

    static WarnIcon() {
        return "\u26A0";
    }

    static SuccessIcon() {
        return "\u2705";
    }
}
module.exports = MessageHelper;