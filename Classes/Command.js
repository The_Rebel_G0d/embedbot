const util = require("util");
class Command {
    constructor(options) {
        let opts = Object.assign({}, {
            trigger: "",
            aliases: [],
            disabled: false,
            handle: (message, message_helper, bot) => {}
        }, options);

        this.trigger = opts.trigger;
        this.aliases = opts.aliases;
        this.disabled = opts.disabled;
        this.handle = opts.handle;
    }
}
module.exports = Command;