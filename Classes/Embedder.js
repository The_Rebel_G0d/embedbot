const async = require("asyncawait/async"),
    await = require("asyncawait/await"),
    Request = require("./Request");
class Embedder {
    constructor() {}

    Embed(url, channel) {
        return Embedder.Embed(channel, url);
    }

    static async Embed(channel, urls, text="", silent = false, otherFields = []) {
        if (!channel.send) throw new Error("Invalid channel supplied");
        if (!urls || !urls.length) throw new Error("Invalid URL supplied");
        if (!Array.isArray(urls)) urls = [urls];
        for (let i = 0; i < urls.length; i++) {
            if (i !== 0) await _wait(750);
            await _embed(channel, urls[i], text, silent, otherFields);
        }
    }

    static async EmbedEmote(channel, urls, emoteCode) {
        if (!channel.send) throw new Error("Invalid channel supplied");
        if (!urls || !urls.length) throw new Error("Invalid URL supplied");
        if (!Array.isArray(urls)) urls = [urls];
        for (let i = 0; i < urls.length; i++) {
            if (i !== 0) await _wait(750);
            await _embed(channel, urls[i], `*Emote: \`${emoteCode}\`*`, true);
        }
    }
}
module.exports = Embedder;

async function _embed(channel, url, text = "", silent = false, otherFields = []) {
    return new Promise((resolve, reject) => {
        new Request(url).getRaw().then(data => {
            let ftype = (require("file-type"))(data);
            if (ftype && ftype.mime.toLowerCase().startsWith("image/")) {
                let embed = {
                    embed: {
                        image: {url: "attachment://image." + ftype.ext},
                        color: 0xFFFF00,
                        fields: [
                            {
                                name: "Original URL",
                                value: url,
                                inline: false
                            }
                        ].concat(otherFields || [])
                    },
                    files: [
                        {attachment: data, name: "image." + ftype.ext}
                    ]
                };

                if (silent === true) {
                    delete embed.embed.color;
                    delete embed.embed.fields;
                }

                if (text && text.length) {
                    channel.send(text, embed).then(() => resolve()).catch(reject);
                } else {
                    channel.send(embed).then(() => resolve()).catch(reject);
                }

            } else logger.debug("Embedder#_embed: ftype check failed");
        }).catch(reject);
    });
}

async function _wait(ms) {
    return new Promise(function(resolve) {
        setTimeout(resolve, ms);
    });
}